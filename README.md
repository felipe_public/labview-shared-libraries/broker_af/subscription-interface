# Subscription Interface for Broker AF

LabVIEW Library for the subscription interface used by the Broker Actor. This library provides methods for actor to subscribe or unsubscribe from topic or content message based pattern.

[![pipeline status](https://gitlab.com/broker_af/message-transport.lvlib/badges/main/pipeline.svg)](https://gitlab.com/broker_af/message-transport.lvlib/-/commits/main)  [![License](https://img.shields.io/badge/license-BSD3-blue)](https://img.shields.io/badge/license-BSD3-blue)

## Installation
- Use with SDM or drop this library into the LabVIEW Project Explorer.

## Usage
- Subscribe or Unsubscribe Usage

![Subscribe Usage](/docs/snippets/subscribe usage.png)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
BSD3

# Author
Felipe Pinheiro Silva
